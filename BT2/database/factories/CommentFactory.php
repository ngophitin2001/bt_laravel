<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class CommentFactory extends Factory
{
    private static $id = 1;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'commentId' => self::$id++,
            'commentContent' => $this->faker->text(),
            'postId' =>  $this->faker->randomElement(['1', '2']),
            'userId' => $this->faker->randomElement(['1', '2']),
        ];
    }
}
