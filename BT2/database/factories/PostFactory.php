<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;

class PostFactory extends Factory
{
    private static $id = 1;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'postId' => self::$id++,
            'userId' => $this->faker->randomElement(['1', '2']),   // để id lấy ngẫu nhiên từ model user
            'postContent' => $this->faker->text(),
        ];
    }
}
