<?php

namespace App\Http\Controllers;

use Dotenv\Exception\ValidationException;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    protected $user;
    public function __construct()
    {
        $this->user = new User();
    }
    public function register(Request $request){
        
        try{
            $validatedData = $request->validate([
                'user_name' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'password' => 'required|string|min:3',
            ]);
    
            $validatedData['password'] = Hash::make($validatedData['password']);
            $result = $this->user->register($validatedData);
    
            return $result;
        } catch (ValidationException $e) {
            return response()->json(['Error' => 'validate error'], 400);
        }
    }

    public function login(Request $request){
        $data = [
            'user_name' => $request->input('user_name'),
            'password' => $request->input('password'),
        ];
        $result = $this->user->login($data);
        return $result;
    }
}
