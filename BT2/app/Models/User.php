<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function register($data){
        $result = $this->insert([
            'name' => $data['user_name'],
            'email' => $data['email'],
            'password' => $data['password'],
        ]);
        if($result){
            return response()->json(['success' => 'Successfully Register'], 200);
        }else{
            return response()->json(['error' => 'Error Register'], 401);
        }
    }

    public function login($data){
        $credentials = [
            'name' => $data["user_name"],
            'password' => $data["password"]
        ];
        
        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return response()->json(['success' => 'Logged in successfully', 'data' => $credentials], 200);
    }
}
